﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public AudioSource SFX;
    public bool isQ;
    public bool isW;
    public bool isE;

    public GameObject Qghost;
    public GameObject Wghost;
    public GameObject Eghost;

    public GameObject Q;
    public GameObject W;
    public GameObject E;
    public GameObject Qb;
    public GameObject Wb;
    public GameObject Eb;

    float Qx;
    float Qy;
    float Qz;

    float Wx;
    float Wy;
    float Wz;

    float Ex;
    float Ey;
    float Ez;

    void Start()
    {
        isQ = true;
        isW = false;
        isE = false;
        Qghost.SetActive(false);
        Wghost.SetActive(true);
        Eghost.SetActive(true);

    }
    
    void FixedUpdate()
    {
        
        if (Input.GetKey(KeyCode.Q) && (isW || isE) && isQ == false && !(Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0)))
        {
            Q.SetActive(true);
            W.SetActive(false);
            E.SetActive(false);
            Qb.SetActive(true);
            Wb.SetActive(false);
            Eb.SetActive(false);
            Qghost.SetActive(false);
            Wghost.SetActive(true);
            Eghost.SetActive(true);
            isQ = true;
            isW = false;
            isE = false;
            SFX.Play();


        }
        else if (Input.GetKey(KeyCode.W) && (isQ || isE) && isW == false && !(Input.GetMouseButton(1) || Input.GetMouseButton(0)))
        {
            Q.SetActive(false);
            W.SetActive(true);
            E.SetActive(false);
            Qb.SetActive(false);
            Wb.SetActive(true);
            Eb.SetActive(false);
            Qghost.SetActive(true);
            Wghost.SetActive(false);
            Eghost.SetActive(true);
            isQ = false;
            isW = true;
            isE = false;
            SFX.Play();


        }
        else if (Input.GetKey(KeyCode.E) && (isW || isQ) && isE == false && !(Input.GetMouseButton(1) || Input.GetMouseButton(0)))
        {
            Q.SetActive(false);
            W.SetActive(false);
            E.SetActive(true);
            Qb.SetActive(false);
            Wb.SetActive(false);
            Eb.SetActive(true);
            Qghost.SetActive(true);
            Wghost.SetActive(true);
            Eghost.SetActive(false);
            isQ = false;
            isW = false;
            isE = true;
            SFX.Play();


        }
    }
}