﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class upDown : MonoBehaviour {

    public AudioSource sfx;
    public Animation anim;
    public bool notCount;

	void OnCollisionEnter (Collision col)
    {
        anim.Play("goDown");
        sfx.Play();
        GameObject udman = GameObject.Find("upDownersManager");
        upDownManager udmanager = udman.GetComponent<upDownManager>();
        if (!notCount) { 
            udmanager.pollote += 1;
        }
    }

}
