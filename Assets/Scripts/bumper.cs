﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumper : MonoBehaviour {

    public Animation anim;
    public AudioSource bumperSFX;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
       

	}

    void OnCollisionEnter(Collision collision)
    {

        anim.Play();
        bumperSFX.Play();
        
    }
}
