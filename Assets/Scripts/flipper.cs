﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flipper : MonoBehaviour
{
    //Min and max angle for the flippers
    public int State1 = 0;
    public int State2 = 90;

    //Flippers' force
    public int Force = 1000;


    public int flipperDamper = 300;

    HingeJoint hinge;
    public string inputKey;


    void Start()
    {
        GetComponent<HingeJoint>().useSpring = true;
        hinge = GetComponent<HingeJoint>();
    }


    void Update()
    {
        //The move magic happens here

        JointSpring spring = new JointSpring();

        spring.spring = Force;

        spring.damper = flipperDamper;

        //The following conditional manages the inputs
        if (Input.GetAxis(inputKey) == 1)
        {
            spring.targetPosition = State2;
            Debug.Log("You pressed the key!");
        }
        else
        {
            spring.targetPosition = State1;
        }
        hinge.spring = spring;
        hinge.useLimits = true;
    }
}