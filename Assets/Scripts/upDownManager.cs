﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class upDownManager : MonoBehaviour {
    public GameObject[] upDowners;
    public int pollote;
    public AudioSource sfx;

    void Start()
    {
        pollote = 0;
    }

    void goUp()
    {
        for (int i = 0; i < upDowners.Length; i++)
        {
            upDowners[i].GetComponent<Animation>().Play("goUp");
            
        }
        pollote = 0;
    }

	void Update () {
		if(pollote == upDowners.Length)
        {
            Invoke("goUp", 0.4f);
            sfx.Play();
            
        }            
    }
}