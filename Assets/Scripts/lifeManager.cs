﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class lifeManager : MonoBehaviour
{

    public Text lifeText;
    private int lifes;
    public GameObject myCanvas;
    public GameObject ball1;
    Vector3 initialPos;
    public AudioSource SFX;

    void Start()
    {
        initialPos = ball1.transform.position;
        lifes = 3;
        SetlifeText();
    }

    void BackToMain()
    {
        SceneManager.LoadScene("MAIN", LoadSceneMode.Single);
    }

    void Update()
    {
        if (lifes <= 0)
        {
            myCanvas.SetActive(true);
            Invoke("BackToMain", 3.0f);  
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("dead"))
        {
            lifes = lifes - 1;
            SetlifeText();
            if(lifes > 0) { 
            ball1.transform.position = initialPos;
                SFX.Play();
            }
        }


    }

    void SetlifeText()
    {
        lifeText.text = "LIFES   " + lifes.ToString();
    }
}