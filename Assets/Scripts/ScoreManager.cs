﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{

    public Text countText;
    private int count;
    private int special;

    void Start()
    {
        count = 0;
        SetCountText();
    }

    void Update()
    {
        if(special >= 4)
        {
            count = count + 900;
            special = 0;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("bouncer"))
        {
            count = count + 100;
            SetCountText();
        }

        if (other.gameObject.CompareTag("upDownTag"))
        {
            count = count + 300;
            SetCountText();
            special++;
        }
    }

    void SetCountText()
    {
        countText.text = "SCORE   " + count.ToString();
    }
}